﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using DataAccess;

namespace Component
{
    public class GeneralComponent
    {
        GeneralDataaccess DA = new GeneralDataaccess();

        public GeneralEntities getDataCustomer(GeneralEntities oGE)
        {
            oGE = DA.getDataCustomer(oGE);

            return oGE;
        }

    }
}
