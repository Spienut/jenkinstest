﻿using PublicClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class HelperFuction
    {
        private SqlTransaction ST;
        private SqlConnection SCon;
        private SqlCommand SCom;
        private SqlDataAdapter SAd;
        private SqlParameter SPar;

        private int i { get; set; }
        private string DBConn = null;

        private DataSet ds;

        GeneralClass gc = new GeneralClass();

        public struct StoreProcArgument
        {
            public string ArgumentName;
            public SqlDbType SQLDT;
            public ParameterDirection ParDir;
            public Object value;
        }

        public StoreProcArgument CreateSPParameter(string AN, ParameterDirection PD, SqlDbType SDT, object val)
        {
            StoreProcArgument ret = default(StoreProcArgument);
            ret.ArgumentName = AN;
            ret.SQLDT = SDT;
            ret.ParDir = PD;
            ret.value = val;
            return ret;

        }

        public DataSet GetDataStoreProcedure(string sp, StoreProcArgument[] SPParameter)
        {
            DBConn = gc.WebConfigConnection();

            SCon = new SqlConnection(DBConn);
            SCom = new SqlCommand();
            SAd = default(SqlDataAdapter);

            ds = new DataSet();

            SCon.Open();
            SCom.Connection = SCon;
            SCom.CommandType = CommandType.StoredProcedure;
            SCom.CommandText = sp;

            SPar = default(SqlParameter);

            for (i = 0; i <= SPParameter.Length - 1; i++)
            {
                SPar = new SqlParameter(SPParameter[i].ArgumentName, SPParameter[i].value);
                SPar.Direction = SPParameter[i].ParDir; SCom.Parameters.Add(SPar);
            }

            SAd = new SqlDataAdapter(SCom);
            SCon.Close();
            SAd.Fill(ds);

            SAd = null;
            SCon = null;

            return ds;
        }

        public DataSet GetDataStoreProcedureCommitRollback(string sp, StoreProcArgument[] SPParameters)
        {
            SAd = default(SqlDataAdapter);
            DataSet ds = new DataSet();


            SCom.Parameters.Clear();

            SqlParameter sqlParam = default(SqlParameter);
            for (int j = 0; j <= SPParameters.Length - 1; j++)
            {
                sqlParam = new SqlParameter(SPParameters[j].ArgumentName, SPParameters[j].value);
                sqlParam.Direction = SPParameters[j].ParDir; SCom.Parameters.Add(sqlParam);
            }


            SCom.CommandTimeout = 0;
            SCom.CommandText = sp;
            //com.ExecuteNonQuery();

            SAd = new SqlDataAdapter(SCom);
            SAd.Fill(ds);
            SAd = null;
            return ds;

        }

        public void BeginTran()
        {


            DBConn = gc.WebConfigConnection();

            SCon = new SqlConnection(DBConn);

            SCon.Open();

            SCom = new SqlCommand();

            SCom.CommandType = CommandType.StoredProcedure;
            SCom.Connection = SCon;

            ST = SCon.BeginTransaction();

            SCom.Transaction = ST;
            SCom.Parameters.Clear();
            SCom.CommandType = CommandType.StoredProcedure;

        }

        public void CommitTran()
        {
            ST.Commit();
            SCon.Close();
            SCon.Dispose();
        }

        public void RollBackTran()
        {
            ST.Rollback();
            SCon.Close();
            SCon.Dispose();
        }
    }
}
