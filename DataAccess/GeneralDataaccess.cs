﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data;

namespace DataAccess
{
    public class GeneralDataaccess
    {
        HelperFuction proses = new HelperFuction();

        public GeneralEntities getDataCustomer(GeneralEntities oGE)
        {
            try
            {
                HelperFuction.StoreProcArgument[] Params = new HelperFuction.StoreProcArgument[1];

                Params[0] = proses.CreateSPParameter("@OrderID", ParameterDirection.Input, SqlDbType.Int, oGE.OrderID);

                oGE.ListDataset = proses.GetDataStoreProcedure(oGE.SPName, Params);
            }
            catch
            {
                throw;
            }

            return oGE;
        }
    }
}
