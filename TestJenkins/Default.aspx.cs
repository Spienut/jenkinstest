﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Component;
using Entities;

public partial class _Default : System.Web.UI.Page
{
    GeneralComponent cc = new GeneralComponent();
    GeneralEntities ec = new GeneralEntities();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        try
        {
            ec.SPName = "CustOrdersDetail";
            ec.OrderID = int.Parse(txtOrderID.Text);//10248
            ec = cc.getDataCustomer(ec);
            gvOrderDetail.DataSource = ec.ListDataset.Tables[0];
            gvOrderDetail.DataBind();
        }
        catch (Exception)
        { 
        
        }
    }
}

