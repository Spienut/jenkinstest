﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class GeneralEntities
    {
        public int OrderID { get; set; }
        public string SPName { get; set; }
        public DataSet ListDataset { get; set; }
    }
}
